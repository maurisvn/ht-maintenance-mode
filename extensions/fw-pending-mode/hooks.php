<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

// add_action( 'page_template', 'fw_reserve_page_template_pending', 99 );
// function fw_reserve_page_template_pending( $page_template ){
// 	$pending = fw()->extensions->get( 'fw-pending-mode' );
// 	$pending_setting = fw_get_db_ext_settings_option('fw-pending-mode');
// 	if( !empty($pending_setting['page']) ){
// 		if( $pending_setting['content'] == '0' ){
// 			//Don't redirect if user is logged in or user is trying to sign up or sign in
// 			if(  !is_login_page() && !is_admin() && !is_user_logged_in() ){
// 				if( $pending_setting['mode'] == 'on' ){
// 					$page_template = $pending->locate_view_path('template-pending');
// 				}
// 			}
// 		    return $page_template;
// 		}
// 	}
// }

function is_login_page() {
    return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
}

add_action('template_redirect','fw_redirect_pending');
function fw_redirect_pending(){
	$pending = fw()->extensions->get( 'fw-pending-mode' );
	$pending_setting = fw_get_db_ext_settings_option('fw-pending-mode');
	if( !empty($pending_setting['page']) && $pending_setting['mode'] == 'on' ){
		$page_id = $pending_setting['page']['0'];
		//Don't redirect if user is logged in or user is trying to sign up or sign in
		if(  !is_login_page() && !is_admin() && !is_user_logged_in() && !is_page( $page_id ) ){
			$url = get_permalink( $page_id );
			wp_redirect($url);
			exit();
		}
	}
}