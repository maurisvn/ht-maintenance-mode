<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'HT Coming Soon', 'mauris' );
$manifest['description'] = __( "Visitor will be redirected to Coming Soon page", 'mauris' );
$manifest['version'] = '1.1';
$manifest['display'] = true;
$manifest['standalone'] = true;
$manifest['thumbnail'] = 'fa fa-hourglass-half';
$manifest['github_update'] = false;
