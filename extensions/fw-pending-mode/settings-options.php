<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'general' => array(
		'type' => 'tab',
		'title' => __('General', 'mauris'),
		'options' => array(
			'mode' => array(
				'type' => 'switch',
				'value' => 'off',
				'label' => __('Enable Under Construction Status', 'mauris'),
				'desc' => __('Whenever visitors come, they will travel to under construction tempplate', 'mauris'),
				'left-choice' => array(
				        'value' => 'off',
				        'label' => __('OFF', 'mauris'),
				    ),
			    'right-choice' => array(
			        'value' => 'on',
			        'label' => __('ON', 'mauris'),
			    ),
			),
			'page' => array(
				'type'  => 'multi-select',
				    'label' => __('Choose Page *', 'mauris'),
				    'desc'  => __('Choose the page to display under construction', 'mauris'),
				    'help'  => __('Leave it empty to use the default template', 'mauris'),
				    'population' => 'posts',
				    'source' => 'page',
				    'limit' => 1,
			),
			// 'content' => array(
			// 	'type' => 'switch',
			// 	'value' => 'off',
			// 	'label' => __('', 'mauris'),
			// 	'desc' => __('Whenever visitors come, they will travel to under construction tempplate', 'mauris'),
			// 	'left-choice' => array(
			// 	        'value' => '0',
			// 	        'label' => __('Use file template', 'mauris'),
			// 	    ),
			//     'right-choice' => array(
			//         'value' => '1',
			//         'label' => __('Use content of that page', 'mauris'),
			//     ),
			// ),
		)
	)
);