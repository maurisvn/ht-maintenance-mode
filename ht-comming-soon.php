<?php
/**
 * Plugin Name: HT Comming Soon
 * Plugin URI:
 * Description: This plugin provides under structure view when the site is being constructing
 * Version: 1.1.1
 * Author: haintheme
 * Author URI: http://haintheme.com
 * Text Domain: mauris
 * License: A short license name. Example: GPL2
 */

function _fw_filter_pending_mode_extensions($locations) {
    $locations[dirname(__FILE__) . '/extensions']
    =
    plugin_dir_url( __FILE__ ) . 'extensions';

    return $locations;
}
add_filter('fw_extensions_locations', '_fw_filter_pending_mode_extensions');
?>